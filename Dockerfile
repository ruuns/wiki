FROM nginx:latest
COPY nginx.conf /etc/nginx/nginx.conf

USER nginx

COPY www /var/www

USER root
